function [maskedFAmap, FAmap, mask, ref, B1] = JK_FAmap_recon(twix, sz)
    %[maskedFAmap, FAmap, mask] = JK_FAmap_recon(twix, sz)
    
    %if the location of the MEAS.dat-file is given instead of the twix
    %struct itself, the function first loads the twix-file
    %
    % Works for 2D multislice and 3D data. 
    % Setting the size sz ([sx, sy]) reconstructs the data to the desired
    %   dimensions (which makes it easier to get the same matrix size as
    %   the relative maps).
    % matthijs.debuck@ndcn.ox.ac.uk

    if ischar(twix) || isstring(twix) 
        twix = mapVBVD(char(twix));
    end
    
    %% Sort & Prepare data
    twix.image.flagRemoveOS = true;
    twix.image.flagDoAverage = true;
    twix.image.flagIgnoreSeg = true;
    k = squeeze(twix.image());
    k = permute(k, [1 3 4 2 5]);
    
    if twix.image.dataSize(5) > 1   %multislice
        [~, order_idx] = sort(twix.hdr.Config.chronSliceIndices(1:twix.image.NSli));
        k = k(:,:,order_idx,:,:);
        k = fftdim(k,3);
    end

    if exist('sz','var') %ensure specified matrix size if required
        if size(k,1) > sz(1)
            k = k(end/2-sz(1)/2+1:end/2+sz(1)/2,:,:,:,:);
        end
        if size(k,2) ~= sz(2)
            k0 = zeros([size(k,1),sz(2),size(k,3),size(k,4),size(k,5)]); %size(k,3:5) only works on newer Matlab versions (>R2019b)
            k0(:,end+1-size(k,2):end,:,:,:) = k;
            k = k0;
        end
    end

    %% Reconstruct data
    imgs = fftdim(k,1:3);

    % Using combined rx - gives same results as AbsB1Map.m
    imgs = permute(rx_combination( permute(imgs,[1 2 3 5 4]) ), [1 2 3 5 4]);
    
    imgratio = real(imgs(:,:,:,2)./imgs(:,:,:,1));
    ratiomask = (imgratio > 1) + (imgratio < -1);
    imgratio(imgratio > 1) = 1;
    imgratio(imgratio < -1) = -1;

    FAmap = acos(imgratio)*180/pi;      % Radians -> Degrees
    FAmap = abs(FAmap);
    
    %% Calculate signal mask (remove background noise)
    mask_img = imgs(:,:,:,1); 
    mask = (imgaussfilt(abs(mask_img),'FilterSize',5) > prctile(abs(mask_img(:)),95)/10);   %Signal threshold: 10% of 95th percentile of signal (no noise measurements available, this seems to work)
    mask(logical(ratiomask)) = 0; %remove values that were changed based on ratio
    maskedFAmap = FAmap.*mask;

    %% From Aaron (AbsB1Map.m): calculate B1/RefV maps
    refV = twix.hdr.Phoenix.sTXSPEC.asNucleusInfo{1}.flReferenceAmplitude;
    satFA = twix.hdr.Phoenix.sWiPMemBlock.adFree{1};

    B1 = FAmap/satFA/refV/0.002; % Hz/V
    refVmap = 1/B1/0.002; % Volt per deg
    
    ref = imgs(:,:,:,1);
end

