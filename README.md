This code reconstructs wide dynamic range multi-channel pTx B1+ maps using
the pipeline described in "HHead-and-neck multi-channel B1+ mapping and RF
shimming of the carotid arteries using a 7T parallel transmit head coil" (de
Buck, Kent, Jezzard, Hess, 2023). It is written for the reconstruction of
twix-data files acquired using our local protocols (on Siemens Magnetom 
7T using a Nova 32Rx/8Tx); for other vendors/sequences, it is shared so 
it can be used as a reference.

For more information, see main.m
