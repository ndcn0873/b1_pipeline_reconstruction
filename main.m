% This code reconstructs wide dynamic range multi-channel pTx B1+ maps using
% the pipeline described in "Head-and-neck multi-channel B1+ mapping and RF
% shimming of the carotid arteries using a 7T parallel transmit head coil" 
% (de Buck, Kent, Jezzard, Hess, 2023). It is written for the reconstruction
% of twix-data files acquired using our local protocols; for other
% vendors/sequences, it is shared so it can be used as a reference.
% 
% The code uses various functions taken from the "mattwixtools" repository
% by Aaron Hess; for convenience, those tools have been included in the
% AaronFunctions_dependencies folder for the scrips that are required to run
% this code. The function nanimresize is a local copy of an openly
% available tool by Chad A. Greene of the University of Texas at 
% Austin's Institute for Geophysics. The reconstructions requires Matlab's
% 'Signal Processing', 'Image Processing', and 'Statistics and Machine
% Learning' Toolboxes. 
% 
% For questions, please contact Matthijs de Buck
% (matthijs.debuck@ndcn.ox.ac.uk / thijsdebuck@live.nl)

%% Select the main folder path
path = uigetdir();

%% Select required files within folder
% This is written for the names/formats of our local acquisitions. Absolute
% B1+ acquisitions (Kent et al., 2022) are selected based on the string
% segment "JK_TFL_B1MAP_3D"; gre-based relative maps are selected based on
% the segment "gre_map_ath_Ref" followed by the applied reference voltage
% (here: 50 V / 100 V / 175V). 
%
% All data is selected in the Siemens raw data (twix) format.

AbsMapFiles = dir(path+"*JK_TFL_B1MAP_3D*.dat");
RelMapFiles = {dir(path+"*gre_map_ath_Ref50*.dat"),dir(path+"*gre_map_ath_Ref100*.dat"),dir(path+"*gre_map_ath_Ref175*.dat")};

%% Reconstruct gre relative maps (from twix data)
disp('--- Reconstructing multi-voltage relative maps... (takes a minute or two) ---')
[map3x, ref3x, ~, ~] = RelMap({FIDfix(path+"/"+RelMapFiles{1}.name), ...
    FIDfix(path+"/"+RelMapFiles{2}.name), FIDfix(path+"/"+RelMapFiles{3}.name)});

%% Sort Absolute map files: based on reference voltages & shims 
% Each absolute map is reconstructed from 6 separate acquisitions: 2 RF
% shims (CP & CP2) at 3 voltages each

% (note: based on our local default filenames only!)
for index = 1:numel(AbsMapFiles) 
    if (contains(AbsMapFiles(index).name,'cp2')) || contains(AbsMapFiles(index).name,'CP2')
        if contains(AbsMapFiles(index).name,'Ref50'), CP2Files{1} = FIDfix(path+"\"+AbsMapFiles(index).name);
        elseif contains(AbsMapFiles(index).name,'Ref100'), CP2Files{2} = FIDfix(path+"\"+AbsMapFiles(index).name);
        elseif contains(AbsMapFiles(index).name,'Ref175'), CP2Files{3} = FIDfix(path+"\"+AbsMapFiles(index).name);
        end
    elseif (contains(AbsMapFiles(index).name,'cp')) || contains(AbsMapFiles(index).name,'CP')
        if contains(AbsMapFiles(index).name,'Ref50'), CPFiles{1} = FIDfix(path+"\"+AbsMapFiles(index).name);
        elseif contains(AbsMapFiles(index).name,'Ref100'), CPFiles{2} = FIDfix(path+"\"+AbsMapFiles(index).name);
        elseif contains(AbsMapFiles(index).name,'Ref175'), CPFiles{3} = FIDfix(path+"\"+AbsMapFiles(index).name);
        end
    end
end

if (sum(strcmp(CPFiles,CPFiles)) ~= 3) || (sum(strcmp(CP2Files,CP2Files)) ~= 3)
    error('No files at all of 50V, 100V, 175V found for CP and/or CP2')
end

% Reconstruct all maps & combine based on different voltages
disp('--- Reconstructing multi-Voltage absolute maps for both shims + applying B1TIAMO & B0-correction... (takes ~20 seconds) ---')
parfor GPU = 1:2
    if GPU == 1, [HzperVboth(:,:,:,GPU), refboth(:,:,:,GPU), ~] = FAMap_DiffVcombine(CPFiles{1}, CPFiles{2}, CPFiles{3}, size(map3x));
    elseif GPU == 2, [HzperVboth(:,:,:,GPU), refboth(:,:,:,GPU), ~] = FAMap_DiffVcombine(CP2Files{1}, CP2Files{2}, CP2Files{3}, size(map3x));
    end
end

% Apply B1TIAMO
[HzperV_ch, refTIAMO] = B1TIAMO(HzperVboth(:,:,:,1),refboth(:,:,:,1),HzperVboth(:,:,:,2),refboth(:,:,:,2),map3x,2); % B1TIAMO combination

%Load + resize (to match other data) unwrapped B0 data (Note: this data has
%already been loaded / unwrapped with PRELUDE
if path(end-2) == 'b', sub = path(end-1); else, sub = path(end-2:end-1); end
load(path+"B0_deltaFreq_prelude_sub"+sub+".mat"), clear delta_freq_orig
temp = zeros(size(ref3x));
for i = 1:size(ref3x,3)
    temp(:,:,i) = nanimresize(double(delta_freq_prel(:,:,i)), [size(temp,1), size(temp,2)]);
end
delta_freq_prel = temp; clear temp

%Calculate frequency response
ts = zeros(1,1e6);              %1e6 us
ts(end/2-250:end/2+249) = 1;    %500 us rect pulse
ts = ts / sum(ts);              %normalize (max FreqResp = 1)
FreqResp = abs(fft(ts));        %Frequency Response (for 1->2000Hz)

% Correct for freq response (second line looks horrible, but simply takes
% all the values in delta_freq_prel and assigns the correspoinding
% freq-response scaling factor into scalingMap)
scalingMap = ones(size(delta_freq_prel));  
scalingMap(~isnan(delta_freq_prel)) = FreqResp(ceil(abs(delta_freq_prel(~isnan(delta_freq_prel(:))))));
HzperV_ch = HzperV_ch ./ scalingMap;

% Done! HzperV_ch is the final, reconstructed, multi-channel B1+ dataset.