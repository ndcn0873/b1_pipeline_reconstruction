function FID = FIDfix(FID)
%Fix file identifiers when using mac/PC ("/" <-> "\")
%   matthijs.debuck@ndcn.ox.ac.uk

if ismac
    FID = char(strrep(FID, "\", "/"));
elseif ispc
    FID = char(strrep(FID, "/", "\"));
end 

end

