function array = sos(array,dim)
%   sos (array, dim):
%   Computes the root-sum-of-squares of the values in the array along the 
%   dimension dim.
%   matthijs.debuck@ndcn.ox.ac.uk
    array = squeeze(sum(abs(array.^2),dim).^0.5);
end

