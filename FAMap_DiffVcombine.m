function [HzperV_comb, ref_comb, masks] = FAMap_DiffVcombine(file_lowV, file_mediumV, file_highV, sz)
% [HzperV_comb, ref_comb] = FAMap_DiffVcombine(file_lowV, file_mediumV, file_highV, sz)
%   Reconstructs 3 absolute B1+ acq's with increasing ref voltages. They're
%   reconstructed using JK_FAmap_recon and then combined to only keep the
%   accurate values from each of the three datasets. Make sure that the
%   correct order (50V -> 100V -> 175V) of the three input files is used.
%   Might update the script later to check that automatically, but it only
%   works for this order atm. Likewise for using different voltage values
%   (and, as a result, different dynamic ranges per acquisition: the
%   calculations work the same, but the thresholds used here would change.)
%   
%   Filenames should be given as the locations of the .dat-files (twix). 
%   sz indicates the in-plane resolution of the reconstruction (to make it
%       easier to match it to relative maps).
%
% matthijs.debuck@ndcn.ox.ac.uk

% Reconstruct 3 datasets using JK_FAmap_recon
[FA1, ~, mask1, ref1, HzperV1] = JK_FAmap_recon(file_lowV, sz);
[FA2, ~, mask2, ref2, HzperV2] = JK_FAmap_recon(file_mediumV, sz);
[FA3, ~, mask3, ref3, HzperV3] = JK_FAmap_recon(file_highV, sz);

%% Ref images: take magnitude + normalize
ref1 = abs(ref1); ref2 = abs(ref2); ref3 = abs(ref3);

if prctile(abs(ref1(:)),95) > prctile(abs(ref3(:)),95)  
    warning('It looks like the order of the .dat files might be wrong! Make sure to check')
end

low_sig_mask1 = logical((ref1<0.1*max(ref1(:))) .* (ref1*1.5 < ref3) .* (HzperV1 > HzperV2));
low_sig_mask2 = logical((ref2<0.1*max(ref2(:))) .* (ref2*1.2 < ref3) .* (HzperV2 > HzperV3));

ref1 = ref1/max(ref1(:)); 
ref2 = ref2/max(ref2(:)); 
ref3 = ref3/max(ref3(:)); 

combmask = logical(~mask1 .* ~mask2 .* ~mask3); %combined original inverse masks
std_dev = std([ref1(combmask);ref2(combmask);ref3(combmask)]);

%% Criterion 1: There has to be a minimum amount of signal in the ref-images
excmask1 = ref1.*mask1 < std_dev;    %excmask = exclusion mask
excmask1(low_sig_mask1) = true;      %partial fix for low-intensity regions 
excmask2 = ref2.*mask2 < std_dev;
excmask2(low_sig_mask2) = true;      %partial fix for low-intensity regions 
excmask3 = ref3.*mask3 < std_dev;

[excmask1, excmask2, excmask3] = remove_singles(excmask1, excmask2, excmask3);

%% Criterion 2: Exclude values outside of the respective dynamic ranges
excmask1(HzperV1 < 4.444) = 1;
excmask2(HzperV2 < 2.222) = 1; excmask2(HzperV2 > 6.667) = 1; 
excmask3(HzperV2 > 3.810) = 1; 

[excmask1, excmask2, excmask3] = remove_singles(excmask1, excmask2, excmask3);

%% Criterion 3: If there is overlap, find the most reliable value
% 50V and 175V overlap: very few voxels (< 1 in 1000), all at edge -->
% probably unreliable partial volume values, so remove (from all 3)
overlap13 = ((~excmask1 + ~excmask3) > 1);
excmask1(overlap13) = 1; excmask2(overlap13) = 1; excmask3(overlap13) = 1; 

% 50V and 100V overlap: if in upper half of overlapping dynamic range (Hz/V
% > 5.5555 = 50/9), 50V gives more reliable values. Otherwise, take mean.
overlap12 = ((~excmask1 + ~excmask2) > 1);
mean12 = (HzperV1+HzperV2)/2;
excmask2(logical(overlap12 .* (mean12 > 50/9))) = 1;

% 100V and 175V overlap: mainly lower brain and skull; if in upper half of 
% overlapping dynamic range (Hz/V > 3), 50V gives more reliable values. 
% Otherwise, take mean.
overlap23 = ((~excmask2 + ~excmask3) > 1);
mean23 = (HzperV2+HzperV3)/2;
excmask3(logical(overlap23 .* (mean23 > 3))) = 1;

[excmask1, excmask2, excmask3] = remove_singles(excmask1, excmask2, excmask3);

%% Combine maps
HzperV1(excmask1) = NaN; HzperV2(excmask2) = NaN; HzperV3(excmask3) = NaN;
HzperV_comb = nanmean(cat(4,HzperV1,HzperV2,HzperV3),4);
HzperV_comb(isnan(HzperV_comb)) = 0;

ref1(excmask1) = NaN; ref2(excmask2) = NaN; ref3(excmask3) = NaN; 
ref_comb = nanmean(cat(4,ref1,ref2,ref3),4);
ref_comb(isnan(ref_comb)) = 0;

masks = cat(4,excmask1,excmask2,excmask3);
end

function [excmask1, excmask2, excmask3] = remove_singles(excmask1, excmask2, excmask3)
%remove values which are (a) part of a very small island, and (b) duplicate
%(i.e. are included from >1 dataset). 
max_sz = 10;

duplicate_locs = (excmask1 + excmask2 + excmask3) < 2;
BW1 = ~bwareaopen(~excmask1,max_sz,6); 
excmask1(logical((BW1-excmask1).*duplicate_locs)) = 1;

duplicate_locs = (excmask1 + excmask2 + excmask3) < 2;
BW2 = ~bwareaopen(~excmask2,max_sz,6); 
excmask2(logical((BW2-excmask2).*duplicate_locs)) = 1;

duplicate_locs = (excmask1 + excmask2 + excmask3) < 2;
BW3 = ~bwareaopen(~excmask3,max_sz,6); 
excmask3(logical((BW3-excmask3).*duplicate_locs)) = 1;
end