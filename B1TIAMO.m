function [RelFA, ref] = B1TIAMO(FA1,ref1,FA2,ref2,map3x,m)
%[RelFA, ref] = B1TIAMO(FA1,ref1,FA2,ref2,map3x,m)
%   Calculates channel-wise absolute B1 maps from a set of relative maps
%   (map3x), and FA-maps in CP-mode (FA1) and CP2-mode (FA2). Uses the
%   method by Brunheim et al. (MRM 2018). 
%   ref1 and ref2 are reference images corresponding to FA1 and FA2.
%   m the exponent used in eq. 4 of the paper. Generally 2 or 3.
% 
%   matthijs.debuck@ndcn.ox.ac.uk

max_val = 130;  %Upper limit of dynamic range (remove higher values)
shim_CP2 = permute(exp(-1j * (1:8) / 4 * pi),[1 3 4 2]);

c1 = abs(min(FA1 ./ sum(map3x,4), max_val));
c2 = abs(min(FA2 ./ sum(map3x .* shim_CP2,4) ,max_val));

% Note: there seems to be an error in the C calculation when using the
% equation in the paper - both the units and the limits behave erratically.
% I've used a slightly modified function which fixes this.
% Cnom = ((c1 .* refCP) .^ m + (c2 .* refneck) .^ m);   %nominator according to paper
Cnom = c1 .* (abs(ref1) .^ m) + c2 .* (abs(ref2) .^ m); %modified equation (fixed brackets?)
Cdenom = (abs(ref1).^m + abs(ref2).^m);
C = Cnom ./ Cdenom;

ref = sos(cat(4,ref1,ref2),4);    %approx combined reference (rSOS)

RelFA = map3x .* C; %combined relative FA maps
RelFA(isnan(RelFA)) = 0; %remove NANs
end

